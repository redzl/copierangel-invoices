<?php
/**
  * This file is part of consoletvs/invoices.
  *
  * (c) Erik Campobadal <soc@erik.cat>
  *
  * For the full copyright and license information, please view the LICENSE
  * file that was distributed with this source code.
  */

namespace ConsoleTVs\Invoices\Traits;

use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * This is the Setters trait.
 *
 * @author Erik Campobadal <soc@erik.cat>
 */
trait Setters
{
    public function version($version)
    {
        $this->version = $version;

        return $this;
    }

    public function po_number($po_number)
    {
        $this->po_number = $po_number;

        return $this;
    }
    public function do_number($do_number)
    {
        $this->do_number = $do_number;

        return $this;
    }
    public function terms($terms)
    {
        $this->terms = $terms;

        return $this;
    }
    public function sales($sales)
    {
        $this->sales = $sales;

        return $this;
    }

    /**
     * Set the invoice type.
     *
     * @method name
     *
     * @param string $name
     *
     * @return self
     */
    public function type($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the invoice name.
     *
     * @method name
     *
     * @param string $name
     *
     * @return self
     */
    public function name($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the invoice number.
     *
     * @method number
     *
     * @param int $number
     *
     * @return self
     */
    public function number($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Set the invoice decimal precision.
     *
     * @method decimals
     *
     * @param int $decimals
     *
     * @return self
     */
    public function decimals($decimals)
    {
        $this->decimals = $decimals;

        return $this;
    }

    /**
     * Set the invoice decimal precision.
     *
     * @method decimals
     *
     * @param int $decimals
     *
     * @return self
     */
    public function tax_percentage($tax_percentage)
    {
        $this->tax_percentage = $tax_percentage;

        return $this;
    }

    /**
     * Set the invoice logo URL.
     *
     * @method logo
     *
     * @param string $logo_url
     *
     * @return self
     */
    public function logo($logo_url)
    {
        $this->logo = $logo_url;

        return $this;
    }

    /**
     * Set the invoice date.
     *
     * @method date
     *
     * @param Carbon $date
     *
     * @return self
     */
    public function date($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set the invoice notes.
     *
     * @method notes
     *
     * @param string $notes
     *
     * @return self
     */
    public function notes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Set the invoice business details.
     *
     * @method business
     *
     * @param array $details
     *
     * @return self
     */
    public function business($details)
    {
        $this->business_details = Collection::make($details);

        return $this;
    }

    public function work_order($work_order)
    {
        $this->work_order = $work_order;

        return $this;
    }

    /**
     * Set the invoice customer details.
     *
     * @method customer
     *
     * @param array $details
     *
     * @return self
     */
    public function customer($details)
    {
        $this->customer_details = Collection::make($details);

        return $this;
    }

    /**
     * Set the invoice currency.
     *
     * @method currency
     *
     * @param string $currency
     *
     * @return self
     */
    public function currency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Set the invoice footnote.
     *
     * @method footnote
     *
     * @param string $footnote
     *
     * @return self
     */
    public function footnote($footnote)
    {
        $this->footnote = $footnote;

        return $this;
    }

    /**
     * Set the invoice due date.
     *
     * @method due_date
     *
     * @param Carbon $due_date
     *
     * @return self
     */
    public function due_date(Carbon $due_date = null)
    {
        $this->due_date = $due_date;
        return $this;
    }

    /**
     * Show/hide the invoice tax.
     *
     * @method with_tax
     *
     * @param boolean $with_tax
     *
     * @return self
     */
    public function with_tax($with_tax)
    {
        $this->with_tax = $with_tax;
        return $this;
    }

    /**
     * Show/hide the invoice pagination.
     *
     * @method with_pagination
     *
     * @param boolean $with_pagination
     *
     * @return self
     */
    public function with_pagination($with_pagination)
    {
        $this->with_pagination = $with_pagination;
        return $this;
    }

    /**
     * Show/hide the invoice custom signature. Only with DO.
     *
     * @method with_custom_signature
     *
     * @param boolean $with_custom_signature
     *
     * @return self
     */
    public function with_custom_signature($with_custom_signature)
    {
        $this->with_custom_signature = $with_custom_signature;
        return $this;
    }

    /**
     * Set the do custom signature. Only with DO.
     *
     * @method custom_signature
     *
     * @param boolean $custom_signature
     *
     * @return self
     */
    public function custom_signature($custom_signature)
    {
        $this->custom_signature = $custom_signature;
        return $this;
    }

    /**
     * Show/hide the invoice computer generated text.
     *
     * @method with_computer_generated_text
     *
     * @param boolean $with_computer_generated_text
     *
     * @return self
     */
    public function with_computer_generated_text($with_computer_generated_text)
    {
        $this->with_computer_generated_text = $with_computer_generated_text;
        return $this;
    }

    /**
     * Duplicate the header on each page.
     *
     * @method duplicate_header
     *
     * @param boolean $duplicate_header
     *
     * @return self
     */
    public function duplicate_header($duplicate_header)
    {
        $this->duplicate_header = $duplicate_header;
        return $this;
    }


    public function customer_name($customer_name)
    {
        $this->customer_name = $customer_name;

        return $this;
    }


    public function company_name($company_name)
    {
        $this->company_name = $company_name;

        return $this;
    }


    public function engineer_eff($engineer_eff)
    {
        $this->engineer_eff = $engineer_eff;

        return $this;
    }


    public function response_time($response_time)
    {
        $this->response_time = $response_time;

        return $this;
    }


    public function service_call_type($service_call_type)
    {
        $this->service_call_type = $service_call_type;

        return $this;
    }


    public function service_call_serial($service_call_serial)
    {
        $this->service_call_serial = $service_call_serial;

        return $this;
    }


    public function work_order_serial($work_order_serial)
    {
        $this->work_order_serial = $work_order_serial;

        return $this;
    }


    public function engineers($engineers)
    {
        $this->engineers = $engineers;

        return $this;
    }


    public function machine($machine)
    {
        $this->machine = $machine;

        return $this;
    }


    public function meter_reading_monochrome($meter_reading_monochrome)
    {
        $this->meter_reading_monochrome = $meter_reading_monochrome;

        return $this;
    }


    public function meter_reading_color($meter_reading_color)
    {
        $this->meter_reading_color = $meter_reading_color;

        return $this;
    }


    public function start_work_order($start_work_order)
    {
        $this->start_work_order = $start_work_order;

        return $this;
    }


    public function end_work_order($end_work_order)
    {
        $this->end_work_order = $end_work_order;

        return $this;
    }


    public function problem_description($problem_description)
    {
        $this->problem_description = $problem_description;

        return $this;
    }


    public function solution($solution)
    {
        $this->solution = $solution;

        return $this;
    }


    public function change_toner($change_toner)
    {
        $this->change_toner = $change_toner;

        return $this;
    }


    public function job_status($job_status)
    {
        $this->job_status = $job_status;

        return $this;
    }

    
}
