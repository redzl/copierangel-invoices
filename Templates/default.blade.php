<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ $invoice->name }}</title>
        <style>
            * {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            h1,h2,h3,h4,h5,h6,p,span,div {
                font-family: DejaVu Sans;
                font-size:10px;
                font-weight: normal;
            }

            th,td {
                font-family: DejaVu Sans;
                font-size:10px;
            }

            .panel {
                margin-bottom: 20px;
                background-color: #fff;
                /*border: 1px solid transparent;*/
                border-radius: 4px;
                -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
                height: 150px;
            }

            .panel-default {
                border-color: #ddd;
            }

            .panel-body {
                padding: 0px;
            }

            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 0px;
                border-spacing: 0;
                border-collapse: collapse;
                background-color: transparent;

            }

            thead  {
                text-align: left;
                display: table-header-group;
                vertical-align: middle;
            }

            th, td  {
                border: 1px solid #ddd;
                padding: 6px;
            }

            .well {
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            }
            .alignMe {
                list-style-type: none;
                margin: 0
            }

            .alignMe b {
                vertical-align: top;
                display: inline-block;
                width: 45%;
                position: relative;
                padding-right: 10px; /* Ensures colon does not overlay the text */
            }

            .alignMe b::after {
                content: ":";
                position: absolute;
                right: 10px;
            }
        </style>
        @if($invoice->duplicate_header)
            <style>
                @page { margin-top: 350px; }
                @page:last {
                }
                header {
                    top: -320px;
                    position: fixed;
                }
            </style>
        @endif
    </head>
    <body>
        <header>
            <div style="position:absolute; left:30pt; width:150pt;">
                <img class="img-rounded" height="100" src="{{ $invoice->logo }}">
            </div>
            <div style="margin-left:150pt;">
                <div style="height: 100px">
                    <div>
                        {!! $invoice->business_details->count() == 0 ? '<i>No business details</i><br />' : '' !!}
                        {{ $invoice->business_details->get('name')." (".$invoice->business_details->get('registration_number').")" }}<br />
                        {{ $invoice->business_details->get('address') }}<br />
                        Tel: {{ $invoice->business_details->get('phone') }}   {{ ($invoice->business_details->get('fax')) ? 'Fax: '.$invoice->business_details->get('fax') : '' }}<br />
                        Email: {{ $invoice->business_details->get('email') }}   Website: {{ $invoice->business_details->get('website') }}<br />
                        <strong>(SST No. {{ $invoice->business_details->get('tax_id') }})</strong>
                    </div>
                </div>
            </div>
            <div style="clear:both; position:relative;">
                <div style="position:absolute; left:0pt; width:250pt; margin-top: 40px;">
                    <h4>Customer Details:</h4>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            {!! $invoice->customer_details->count() == 0 ? '<i>No customer details</i><br />' : '' !!}
                            {{ $invoice->customer_details->get('name') }}<br />
                            {!! nl2br($invoice->customer_details->get('address')) !!}<br />
                            Tel: {{ $invoice->customer_details->get('phone') }}   {{ ($invoice->customer_details->get('fax')) ? 'Fax: '.$invoice->customer_details->get('fax') : '' }}<br />
                        </div>
                    </div>
                </div>
                <div style="margin-left: 300pt;">
                    <div style="height: 150px; width:250pt; margin-top: 55px;">
                        <ul class="alignMe">
                            @if ($invoice->date)
                                <li><b>Date</b> {{ $invoice->date }}</li>
                            @endif
                            @if ($invoice->type == 2)
                                @if ($invoice->number)
                                    <li><b>DO Number</b> {{ $invoice->number }}</li>
                                @endif
                            @else
                                @if ($invoice->number)
                                    <li><b>Invoice</b> {{ $invoice->number }}</li>
                                @endif
                            @endif
                            @if ($invoice->po_number)
                                <li><b>PO Number</b> {{ $invoice->po_number }}</li>
                            @endif
                            @if ($invoice->do_number)
                                @if ($invoice->type == 2)
                                    <li><b>Invoice Number</b> {{ $invoice->do_number }}</li>
                                @else
                                    <li><b>DO Number</b> {{ $invoice->do_number }}</li>
                                @endif
                            @endif
                            @if ($invoice->terms)
                                <li><b>Terms</b> {{ $invoice->terms }}</li>
                            @endif
                            @if ($invoice->sales)
                                <li><b>Sales</b> {{ $invoice->sales }}</li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <main class="main" >
            <h4>Items:</h4>
            @if ($invoice->type == 2)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 10%;">#</th>
                            @if($invoice->shouldDisplayImageColumn())
                                <th>Image</th>
                            @endif
                            <th style="width: 70%;">Description</th>
                            <th style="text-align: center; width: 20%;">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($invoice->items as $item)
                            <tr>
                                <td valign="top" style="text-align: center;">{{ $loop->iteration }}</td>
                                @if($invoice->shouldDisplayImageColumn())
                                    <td>@if(!is_null($item->get('imageUrl'))) <img src="{{ url($item->get('imageUrl')) }}" />@endif</td>
                                @endif
                                <td valign="top">{!! $item->get('name') !!}</td>
                                <td valign="top" style="text-align: center;">{{ $item->get('ammount') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 10%;">#</th>
                            @if($invoice->shouldDisplayImageColumn())
                                <th>Image</th>
                            @endif
                            <th style="width: 50%">Description</th>
                            <th style="text-align: center; width: 10%;">Quantity</th>
                            <th style="text-align: center; width: 15%;">Price (RM)</th>
                            <th style="text-align: center; width: 15%;">Total (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($invoice->items as $item)
                            <tr>
                                <td valign="top" style="text-align: center;">{{ $loop->iteration }}</td>
                                @if($invoice->shouldDisplayImageColumn())
                                    <td>@if(!is_null($item->get('imageUrl'))) <img src="{{ url($item->get('imageUrl')) }}" />@endif</td>
                                @endif
                                <td valign="top">{!! $item->get('name') !!}</td>
                                <td valign="top" style="text-align: center;">{{ $item->get('ammount') }}</td>
                                <td valign="top" style="text-align: right;">{{ $item->get('price') }}</td>
                                <td valign="top" style="text-align: right;">{{ $item->get('totalPrice') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif

        </main>

        <div style="position: relative; height: 250px;">
            <footer style="">
                @if ($invoice->type == 2)
                    <div>
                        <div style="width:100%;">
                            <div style="width:250pt; float:left;">
                                <!--<h4>Notes:</h4>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        {!! nl2br($invoice->notes) !!}
                                    </div>
                                </div>-->
                            </div>
                            <div style="width:200pt; float:right; padding: 15px 0 0 0">
                                {!! nl2br($invoice->custom_signature) !!}
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                _______________________________<br/>
                                <strong>Received By:</strong>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                @else
                    <div style="width:100%;">
                        <div style="width:100%;">
                            <div style="width:250pt; float:left;">
                                @if($invoice->notes)
                                    <h4>Notes:</h4>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            {!! nl2br($invoice->notes) !!}
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div style="width:150pt; float:right;">
                                <h4>Total:</h4>
                                <table class="">
                                    <tbody>
                                        <tr>
                                            <td><b>Subtotal</b></td>
                                            <td>RM {{ $invoice->subTotalPriceFormatted() }}</td>
                                        </tr>
                                        @if ($invoice->with_tax)
                                            <tr>
                                                <td><b>Tax Amount ({{ $invoice->tax_percentage }})</b></td>
                                                <td>RM {{ number_format((float)($invoice->subTotalPriceFormatted() + ($invoice->subTotalPriceFormatted() * $invoice->tax_percentage / 100)), 2, '.', '') }}</td>
                                            </tr>
                                            <!--@foreach($invoice->tax_rates as $tax_rate)
                                                <tr>
                                                    <td>
                                                        <b>
                                                            {{ $tax_rate['name'].' '.($tax_rate['tax_type'] == 'percentage' ? '(' . $tax_rate['tax'] . '%)' : '') }}
                                                        </b>
                                                    </td>
                                                    <td>RM {{ $invoice->taxPriceFormatted((object)$tax_rate) }}</td>
                                                </tr>
                                            @endforeach-->
                                            <tr>
                                                <td><b>TOTAL</b></td>
                                                <td><b>RM {{ number_format((float)($invoice->subTotalPriceFormatted() + ($invoice->subTotalPriceFormatted() * $invoice->tax_percentage / 100)), 2, '.', '') }}</b></td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td><b>TOTAL</b></td>
                                                <td><b>RM {{ $invoice->totalPriceFormatted() }}</b></td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div>
                        @if ($invoice->with_computer_generated_text)
                            <div style="width: 100%; text-align: center;">
                                <p style="text-align: center">This is a computer generated document and does not require a signature</p>
                            </div>
                        @endif
                    </div>
                @endif
            </footer>
        </div>

        <!-- Page count -->
        <script type="text/php">
            if (isset($pdf) && $GLOBALS['with_pagination'] && $PAGE_COUNT >= 1) {
                $pageText = "{PAGE_NUM} of {PAGE_COUNT}";
                $pdf->page_text(($pdf->get_width()/2) - (strlen($pageText) / 2), $pdf->get_height()-20, $pageText, $fontMetrics->get_font("DejaVu Sans, Arial, Helvetica, sans-serif", "normal"), 7, array(0,0,0));
            }
        </script>
    </body>
</html>
