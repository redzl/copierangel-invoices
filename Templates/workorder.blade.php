<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <style>
            * {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            h1,h2,h3,h4,h5,h6,p,span,div { 
                font-family: DejaVu Sans; 
                font-size:10px;
                font-weight: normal;
            }

            th,td { 
                font-family: DejaVu Sans; 
                font-size:10px;
            }

            .panel {
                margin-bottom: 20px;
                background-color: #fff;
                border: 1px solid transparent;
                border-radius: 4px;
                -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
                height: 150px;
            }

            .panel-default {
                border-color: #ddd;
            }

            .panel-body {
                padding: 15px;
            }

            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 0px;
                border-spacing: 0;
                border-collapse: collapse;
                background-color: transparent;

            }

            thead  {
                text-align: left;
                display: table-header-group;
                vertical-align: middle;
            }

            th, td  {
                border: 1px solid #ddd;
                padding: 6px;
            }

            .well {
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            }
            .alignMe {
                list-style-type: none;
                margin: 0
            }

            .alignMe b {
                vertical-align: top;
                display: inline-block;
                width: 45%;
                position: relative;
                padding-right: 10px; /* Ensures colon does not overlay the text */
            }

            .alignMe b::after {
                content: ":";
                position: absolute;
                right: 10px;
            }
        </style>
        @if($invoice->duplicate_header)
            <style>
                @page { margin-top: 160px; }
                @page:last {
                }
                header {
                    top: -120px;
                    position: fixed;
                }
            </style>
        @endif
    </head>
    <body>
        <header>
            <div style="position:absolute; left:0pt; width:150pt;">
                <img class="img-rounded" height="100" src="{{ $invoice->logo }}">
            </div>
            <div style="margin-left:150pt;">
                <div style="height: 100px">
                    <div>
                        {!! $invoice->business_details->count() == 0 ? '<i>No business details</i><br />' : '' !!}
                        {{ $invoice->business_details->get('name')." (".$invoice->business_details->get('registration_number').")" }}<br />
                        {{ $invoice->business_details->get('address') }}<br />
                        Tel: {{ $invoice->business_details->get('phone') }}      {{ ($invoice->business_details->get('fax')) ? 'Fax: '.$invoice->business_details->get('fax') : '' }}<br />
                        Email: {{ $invoice->business_details->get('email') }}   Website: {{ $invoice->business_details->get('website') }}<br />
                        <strong>(SST No. {{ $invoice->business_details->get('tax_id') }})</strong>
                    </div>
                </div>
            </div>
        </header>
        <main class="main" >
            <h4 style="text-align: center; font-weight: bold; font-size: 25px; margin-top: -10px">WORK ORDER</h4>

            <table class="table table-bordered">
                <tbody>
                    @if (!empty(@$invoice->customer_name))
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Customer Name</td>
                            <td valign="top" style="">
                                {{ @$invoice->customer_name }}
                            </td>
                        </tr>
                    @endif
                    @if (!empty(@$invoice->company_name))
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Company Name</td>
                            <td valign="top" style="">
                                {{ @$invoice->company_name }}
                            </td>
                        </tr>
                    @endif
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">{{ @$invoice->service_call_type }} ID</td>
                            <td valign="top" style="">
                                {{ @$invoice->service_call_serial }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Work Order ID</td>
                            <td valign="top" style="">
                                {{ @$invoice->work_order_serial }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Machine</td>
                            <td valign="top" style="">
                                {{ @$invoice->machine }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Meter Reading (Monochrome)</td>
                            <td valign="top" style="">
                                {{ @$invoice->meter_reading_monochrome }}
                            </td>
                        </tr>
                        @if (@$invoice->meter_reading_color > 0)
                            <tr>
                                <td valign="top" style="text-align: left; font-weight: bold">Meter Reading (Color)</td>
                                <td valign="top" style="">
                                    {{ @$invoice->meter_reading_color }}
                                </td>
                            </tr>
                        @endif
                        @if (@$invoice->response_time)
                            <tr>
                                <td valign="top" style="text-align: left; font-weight: bold">Response Time</td>
                                <td valign="top" style="">
                                    {{ @$invoice->response_time }}
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Engineer Efficiency</td>
                            <td valign="top" style="">
                                {{ @$invoice->engineer_eff }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Start Work Order</td>
                            <td valign="top" style="">
                                {{ @$invoice->start_work_order }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">End Work Order</td>
                            <td valign="top" style="">
                                {{ @$invoice->end_work_order }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Job Description</td>
                            <td valign="top" style="">
                                {{ @$invoice->problem_description }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Solution</td>
                            <td valign="top" style="">
                                {{ @$invoice->solution }}
                            </td>
                        </tr>
                        <!--<tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Change Toner</td>
                            <td valign="top" style="">
                                {!! @$invoice->change_toner !!}
                            </td>
                        </tr>-->
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Job Status</td>
                            <td valign="top" style="">
                                {{ @$invoice->job_status }}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: left; font-weight: bold">Engineers</td>
                            <td valign="top" style="">
                                {{ @$invoice->engineers }}
                            </td>
                        </tr>
                </tbody>
            </table>
    
            @if (count($invoice->part_requests) !== 0)
                <table class="table table-bordered" style="margin: 25px 0">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 5%;">#</th>
                            <th style="width: 15%;">ID</th>
                            <th style="width: 25%;">Part Name</th>
                            <th style="width: 55%;">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($invoice->part_requests as $item)
                            <tr>
                                <td valign="top" style="text-align: center;">{{ $loop->iteration }}</td>
                                <td valign="top">{!! $item->get('part_request_id') !!}</td>
                                <td valign="top">{!! $item->get('name') !!}</td>
                                <td valign="top">{!! $item->get('description') !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif

        </main>



        <div style="position: relative; height: 120px; margin: 35px 0 0">
            <footer style="position: absolute; bottom: 0;">
                <div>
                    <div style="width:100%;">
                        <div style="width:200pt; float:right; padding: 15px 0 0 0">
                            Acknowledged by:
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            _______________________________<br/>
                            <strong>Date:</strong>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div>
                    @if ($invoice->with_computer_generated_text)
                        <div style="width: 100%; text-align: center;">
                            <p style="text-align: center">This is a computer generated document and does not require a signature</p>
                        </div>
                    @endif
                </div>
            </footer>
        </div>


        <!-- Page count -->
        <script type="text/php">
            if (isset($pdf) && $GLOBALS['with_pagination'] && $PAGE_COUNT >= 1) {
                $pageText = "{PAGE_NUM} of {PAGE_COUNT}";
                $pdf->page_text(($pdf->get_width()/2) - (strlen($pageText) / 2), $pdf->get_height()-20, $pageText, $fontMetrics->get_font("DejaVu Sans, Arial, Helvetica, sans-serif", "normal"), 7, array(0,0,0));
            }
        </script>
    </body>
</html>
