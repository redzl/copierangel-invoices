<?php
/**
  * This file is part of consoletvs/invoices.
  *
  * (c) Erik Campobadal <soc@erik.cat>
  *
  * For the full copyright and license information, please view the LICENSE
  * file that was distributed with this source code.
  */

namespace ConsoleTVs\Invoices\Classes;

use Carbon\Carbon;
use ConsoleTVs\Invoices\Traits\Setters;
use Illuminate\Support\Collection;
use Storage;

/**
 * This is the Invoice class.
 *
 * @author Erik Campobadal <soc@erik.cat>
 */
class Invoice
{
    use Setters;


    /**
     * Doc version.
     *
     * @var int
     */
    public $version;

    /**
     * 
     * @var string
     */
    public $po_number;

    /**
     * 
     * @var string
     */
    public $do_number;

    /**
     * 
     * @var string
     */
    public $terms;

    /**
     * 
     * @var string
     */
    public $sales;

    /**
     * Invoice type.
     *
     * @var string
     */
    public $type;

    /**
     * Invoice name.
     *
     * @var string
     */
    public $name;

    /**
     * Invoice template.
     *
     * @var string
     */
    public $template;

    /**
     * Invoice item collection.
     *
     * @var Illuminate\Support\Collection
     */
    public $items;

    /**
     * work_order.
     *
     * @var array
     */
    public $work_order;

    /**
     * Invoice item collection.
     *
     * @var Illuminate\Support\Collection
     */
    public $part_requests;

    /**
     * Invoice currency.
     *
     * @var string
     */
    public $currency;

    /**
     * Invoice number.
     *
     * @var int
     */
    public $number = null;

    /**
     * Invoice decimal precision.
     *
     * @var int
     */
    public $decimals;

    /**
     * Invoice decimal precision.
     *
     * @var int
     */
    public $tax_percentage;

    /**
     * Invoice logo.
     *
     * @var string
     */
    public $logo;

    /**
     * Invoice Logo Height.
     *
     * @var int
     */
    public $logo_height;

    /**
     * Invoice Date.
     *
     * @var Carbon\Carbon
     */
    public $date;

    /**
     * Invoice Notes.
     *
     * @var string
     */
    public $notes;

    /**
     * Invoice Business Details.
     *
     * @var array
     */
    public $business_details;

    /**
     * Invoice Customer Details.
     *
     * @var array
     */
    public $customer_details;

    /**
     * Invoice Footnote.
     *
     * @var array
     */
    public $footnote;

    /**
     * Invoice Tax Rates Default.
     *
     * @var array
     */
    public $tax_rates;

    /**
     * Invoice Due Date.
     *
     * @var Carbon\Carbon
     */
    public $due_date = null;

    /**
     * Invoice pagination.
     *
     * @var boolean
     */
    public $with_pagination;

    /**
     * 
     *
     * @var boolean
     */
    public $with_tax;

    /**
     * 
     *
     * @var boolean
     */
    public $with_custom_signature;

    /**
     * 
     *
     * @var string
     */
    public $custom_signature;

    /**
     * 
     *
     * @var boolean
     */
    public $with_computer_generated_text;

    /**
     * Invoice header duplication.
     *
     * @var boolean
     */
    public $duplicate_header;

    /**
     * Stores the PDF object.
     *
     * @var Dompdf\Dompdf
     */
    public $pdf;

    /**
     * 
     *
     * @var string
     */
    public $customer_name;

    /**
     * 
     *
     * @var string
     */
    public $company_name;

    /**
     * 
     *
     * @var string
     */
    public $engineers;

    /**
     * 
     *
     * @var string
     */
    public $response_time;

    /**
     * 
     *
     * @var string
     */
    public $engineer_eff;

    /**
     * 
     *
     * @var string
     */
    public $service_call_type;

    /**
     * 
     *
     * @var string
     */
    public $service_call_serial;

    /**
     * 
     *
     * @var string
     */
    public $work_order_serial;

    /**
     * 
     *
     * @var string
     */
    public $machine;

    /**
     * 
     *
     * @var string
     */
    public $meter_reading_monochrome;

    /**
     * 
     *
     * @var string
     */
    public $meter_reading_color;

    /**
     * 
     *
     * @var string
     */
    public $start_work_order;

    /**
     * 
     *
     * @var string
     */
    public $end_work_order;

    /**
     * 
     *
     * @var string
     */
    public $problem_description;

    /**
     * 
     *
     * @var string
     */
    public $solution;

    /**
     * 
     *
     * @var string
     */
    public $change_toner;

    /**
     * 
     *
     * @var string
     */
    public $job_status;

    /**
     * Create a new invoice instance.
     *
     * @method __construct
     *
     * @param string $name
     */
    public function __construct($name = 'Invoice')
    {
        $this->version = 0;
        $this->type = 1;
        $this->name = $name;
        $this->template = 'default';
        $this->items = Collection::make([]);
        $this->work_order = [];
        $this->part_requests = Collection::make([]);
        $this->currency = config('invoices.currency');
        $this->decimals = config('invoices.decimals');
        $this->logo = config('invoices.logo');
        $this->logo_height = config('invoices.logo_height');
        $this->date = Carbon::now();
        $this->business_details = Collection::make([]);
        $this->customer_details = Collection::make([]);
        $this->footnote = config('invoices.footnote');
        $this->tax_rates = config('invoices.tax_rates');
        $this->tax_percentage = 0;
        $this->due_date = config('invoices.due_date') != null ? Carbon::parse(config('invoices.due_date')) : null;
        $this->with_pagination = config('invoices.with_pagination');
        $this->with_custom_signature = false;
        $this->with_tax = false;
        $this->custom_signature = "";
        $this->with_computer_generated_text = false;
        $this->duplicate_header = config('invoices.duplicate_header');

        $this->customer_name = "";
        $this->company_name = "";
        $this->engineer_eff = "";
        $this->response_time = "";
        $this->service_call_type = "";
        $this->service_call_serial = "";
        $this->work_order_serial = "";
        $this->engineers = "";
        $this->machine = "";
        $this->meter_reading_monochrome = "";
        $this->meter_reading_color = "";
        $this->start_work_order = "";
        $this->end_work_order = "";
        $this->problem_description = "";
        $this->solution = "";
        $this->change_toner = "";
        $this->job_status = "";
    }

    /**
     * Return a new instance of Invoice.
     *
     * @method make
     *
     * @param string $name
     *
     * @return ConsoleTVs\Invoices\Classes\Invoice
     */
    public static function make($name = 'Invoice')
    {
        return new self($name);
    }

    /**
     * Select template for invoice.
     *
     * @method template
     *
     * @param string $template
     *
     * @return self
     */
    public function template($template = 'default')
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Adds an item to the invoice.
     *
     * @method addItem
     *
     * @param string $name
     * @param int    $price
     * @param int    $ammount
     * @param string $id
     * @param string $imageUrl
     *
     * @return self
     */
    public function addItem($name, $price, $ammount = 1, $id = '-', $imageUrl = null)
    {
        $this->items->push(Collection::make([
            'name'       => $name,
            'price'      => $price,
            'ammount'    => $ammount,
            'totalPrice' => number_format(bcmul($price, $ammount, $this->decimals), $this->decimals),
            'id'         => $id,
            'imageUrl'   => $imageUrl,
        ]));

        return $this;
    }

    public function addPartRequest($part_request_id, $name, $description)
    {
        $this->part_requests->push(Collection::make([
            'part_request_id'       => $part_request_id,
            'name'      => $name,
            'description'    => $description
        ]));

        return $this;
    }

    /**
     * Pop the last invoice item.
     *
     * @method popItem
     *
     * @return self
     */
    public function popItem()
    {
        $this->items->pop();

        return $this;
    }

    /**
     * Pop the last invoice item.
     *
     * @method popPartRequest
     *
     * @return self
     */
    public function popPartRequest()
    {
        $this->part_requests->pop();

        return $this;
    }

    /**
     * Return the currency object.
     *
     * @method formatCurrency
     *
     * @return stdClass
     */
    public function formatCurrency()
    {
        $currencies = json_decode(file_get_contents(__DIR__.'/../Currencies.json'));
        $currency = $this->currency;

        return $currencies->$currency;
    }

    /**
     * Return the subtotal invoice price.
     *
     * @method subTotalPrice
     *
     * @return int
     */
    private function subTotalPrice()
    {
        return $this->items->sum(function ($item) {
            return bcmul($item['price'], $item['ammount'], $this->decimals);
        });
    }

    /**
     * Return formatted sub total price.
     *
     * @method subTotalPriceFormatted
     *
     * @return int
     */
    public function subTotalPriceFormatted()
    {
        return number_format($this->subTotalPrice(), $this->decimals);
    }

    /**
     * Return the total invoce price after aplying the tax.
     *
     * @method totalPrice
     *
     * @return int
     */
    private function totalPrice()
    {
        return bcadd($this->subTotalPrice(), $this->taxPrice(), $this->decimals);
    }

    /**
     * Return formatted total price.
     *
     * @method totalPriceFormatted
     *
     * @return int
     */
    public function totalPriceFormatted()
    {
        return number_format($this->totalPrice(), $this->decimals);
    }

    /**
     * taxPrice.
     *
     * @method taxPrice
     *
     * @return float
     */
    private function taxPrice(Object $tax_rate = null)
    {
        if (is_null($tax_rate)) {
            $tax_total = 0;
            foreach($this->tax_rates as $taxe){
                if ($taxe['tax_type'] == 'percentage') {
                    $tax_total += bcdiv(bcmul($taxe['tax'], $this->subTotalPrice(), $this->decimals), 100, $this->decimals);
                    continue;
                }
                $tax_total += $taxe['tax'];
            }
            return $tax_total;
        }
        
        if ($tax_rate->tax_type == 'percentage') {
            return bcdiv(bcmul($tax_rate->tax, $this->subTotalPrice(), $this->decimals), 100, $this->decimals);
        }

        return $tax_rate->tax;
    }

    /**
     * Return formatted tax.
     *
     * @method taxPriceFormatted
     *
     * @return int
     */
    public function taxPriceFormatted($tax_rate)
    {
        return number_format($this->taxPrice($tax_rate), $this->decimals);
    }

    /**
     * Generate the PDF.
     *
     * @method generate
     *
     * @return self
     */
    public function generate()
    {
        $this->pdf = PDF::generate($this, $this->template);

        return $this;
    }

    /**
     * Downloads the generated PDF.
     *
     * @method download
     *
     * @param string $name
     *
     * @return response
     */
    public function download($name = 'invoice')
    {
        $this->generate();

        return $this->pdf->stream($name);
    }

    /**
     * Save the generated PDF.
     *
     * @method save
     *
     * @param string $name
     *
     */
    public function save($name = 'invoice.pdf')
    {
        $invoice = $this->generate();

        Storage::put($name, $invoice->pdf->output());
    }

    /**
     * Show the PDF in the browser.
     *
     * @method show
     *
     * @param string $name
     *
     * @return response
     */
    public function show($name = 'invoice')
    {
        $this->generate();

        return $this->pdf->stream($name, ['Attachment' => false]);
    }

    /**
     * Return true/false if one item contains image.
     * Determine if we should display or not the image column on the invoice.
     * 
     * @method shouldDisplayImageColumn
     *
     * @return boolean
     */
    public function shouldDisplayImageColumn()
    {
        foreach($this->items as $item){
            if($item['imageUrl'] != null){
                return true;
            }
        }
    }
}
