<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '778be09a2032b4b942a59ed9d98a9515f1c49b78',
    'name' => 'consoletvs/invoices',
  ),
  'versions' => 
  array (
    'consoletvs/invoices' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '778be09a2032b4b942a59ed9d98a9515f1c49b78',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.1.x-dev',
      'version' => '2.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'c6a0da4f0e06aa5cd83a2c1a4e449fae98c8bad7',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v0.8.6',
      'version' => '0.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db91d81866c69a42dad1d2926f61515a1e3f42c5',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => '7.x-dev',
      'version' => '7.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'cf94ed8fbaeb26906bb42b24377dbb061b97a096',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => '7.x-dev',
      'version' => '7.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '7d964384f0283bd7525ae7b5baa7ad32e5503b8e',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => '7.x-dev',
      'version' => '7.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6f64db49dbfd490c6e30c983964543a054882faf',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => '7.x-dev',
      'version' => '7.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2013f94a3a7dff008be54884774548e3c222c3e8',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => '7.x-dev',
      'version' => '7.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '5535c57caf5e51a399b8d597544038d05d1315a1',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => '7.x-dev',
      'version' => '7.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '31c35ed36a75c5fbb8a1b5a13cfe2813d7d5d044',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => '2fd2c4a77d58a4e95234c8a61c5df1f157a91bf4',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.2',
      'version' => '0.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6ad461f032145fff5971b5985e5af9e7fa88d8',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '0.3.x-dev',
      ),
      'reference' => '16fd4d717be18d8a604f2d0c3045b35aa2f6b0a5',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '5a7b96b1dda5d957e01bc1bfe77dcca09c5a7474',
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.3.1',
      'version' => '8.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd217848e1396ef962fb1997cf3e2421acba7f796',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '0d639a0943822626290d169965804f79400e6a04',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1df60f0debacffeead546abb2bc8857960f757bf',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => '502490047e9064e6f1bc331ab1d43ca3da68983d',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
  ),
);
